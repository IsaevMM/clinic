package net.clinic.clientmanager.service;

import net.clinic.clientmanager.entity.MedicineEntity;

import java.util.List;

public interface MedicineService {
    public void addMedicine(MedicineEntity medicine);
    public List<MedicineEntity> findAll();
    public void delete(Integer id);
}
