package net.clinic.clientmanager.service;

import net.clinic.clientmanager.entity.UserAuthenticationEntity;

import java.util.List;

public interface UserAuthenticationService {

    public void createUser(UserAuthenticationEntity user);

    public void delereUser(Integer id);

    public void blockUser(Integer userId);

    public void unlockUser(Integer userId);

    public void distributionCards(Integer userId);

    public UserAuthenticationEntity findUser(Integer id);

    public List<UserAuthenticationEntity> findAllUsers();

    public UserAuthenticationEntity findByNickname(String nickname);
}
