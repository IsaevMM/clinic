package net.clinic.clientmanager.service;

import net.clinic.clientmanager.entity.DoctorsEntity;

import java.util.List;

public interface DoctorService {
    public void addDoctor(DoctorsEntity doctor);

    public List<DoctorsEntity> findAll();
    public DoctorsEntity findById(Integer id);

    public void delete(Integer id);
}
