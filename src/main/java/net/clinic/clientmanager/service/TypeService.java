package net.clinic.clientmanager.service;

import net.clinic.clientmanager.entity.TypesEntity;

import java.util.List;

public interface TypeService {
    public void addType(TypesEntity type);
    public List<TypesEntity> findAll();
    public void delete(Integer id);
}
