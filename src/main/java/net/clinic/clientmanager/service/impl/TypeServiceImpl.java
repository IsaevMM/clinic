package net.clinic.clientmanager.service.impl;

import net.clinic.clientmanager.entity.TypesEntity;
import net.clinic.clientmanager.service.TypeService;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;


@Component
public class TypeServiceImpl implements TypeService {
    @PersistenceContext
    private EntityManager em;

    @Transactional
    @Override
    public void addType(TypesEntity type) {
        em.persist(type);
    }

    @Override
    public List<TypesEntity> findAll() {
        return em.createQuery("select t from TypesEntity t").getResultList();
    }

    @Override
    public void delete(Integer id) {

    }
}
