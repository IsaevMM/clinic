package net.clinic.clientmanager.service.impl;

import net.clinic.clientmanager.entity.MedicineEntity;
import net.clinic.clientmanager.service.MedicineService;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;


@Component
public class MedicineServiceImpl implements MedicineService {

    @PersistenceContext
    private EntityManager em;

    @Transactional
    @Override
    public void addMedicine(MedicineEntity medicine) {
        em.persist(medicine);
    }

    @Override
    public List<MedicineEntity> findAll() {
        return em.createQuery("select m from MedicineEntity m").getResultList();
    }

    @Override
    public void delete(Integer id) {

    }
}
