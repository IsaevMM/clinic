package net.clinic.clientmanager.service.impl;

import net.clinic.clientmanager.entity.DoctorsEntity;
import net.clinic.clientmanager.service.DoctorService;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;


@Component
public class DoctorServiceImpl implements DoctorService {


    @PersistenceContext
    private EntityManager em;

    @Transactional
    @Override
    public void addDoctor(DoctorsEntity doctor) {
        em.persist(doctor);
    }

    @Override
    public List<DoctorsEntity> findAll() {
        return em.createQuery("select d from DoctorsEntity d").getResultList();
    }

    @Override
    public DoctorsEntity findById(Integer DOC_ID) {

        return em.find(DoctorsEntity.class, DOC_ID);
    }

    @Override
    public void delete(Integer id) {

    }
}
