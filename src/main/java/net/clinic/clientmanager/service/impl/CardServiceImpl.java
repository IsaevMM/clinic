package net.clinic.clientmanager.service.impl;

import net.clinic.clientmanager.entity.CardsEntity;
import net.clinic.clientmanager.service.CardService;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Component
public class CardServiceImpl implements CardService {

    @PersistenceContext
    private EntityManager em;



    @Transactional
    @Override
    public void createCard(CardsEntity card) {
        em.persist(card);

    }

    @Override
    public CardsEntity findCard(Integer id) {
        return em.find(CardsEntity.class, id);
    }

    @Override
    public List<CardsEntity> findAllCard() {
        return em.createQuery("select c from CardsEntity c").getResultList();
    }

    @Transactional
    @Override
    public void deleteCard(Integer id) {
        CardsEntity card = em.find(CardsEntity.class, id);
        if (card != null) {
            em.remove(card);
        }

    }


}
