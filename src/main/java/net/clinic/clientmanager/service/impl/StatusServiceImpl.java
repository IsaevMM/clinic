package net.clinic.clientmanager.service.impl;

import net.clinic.clientmanager.entity.StatusEntity;
import net.clinic.clientmanager.service.StatusService;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;


@Component
public class StatusServiceImpl implements StatusService {


    @PersistenceContext
    private EntityManager em;

    @Override
    public void addStatus(StatusEntity statusEntity) {

    }

    @Override
    public List<StatusEntity> findAll() {
        return em.createQuery("select s from StatusEntity s").getResultList();
    }

    @Override
    public void delete(Integer id) {

    }
}
