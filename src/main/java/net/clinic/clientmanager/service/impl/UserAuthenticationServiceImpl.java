package net.clinic.clientmanager.service.impl;

import net.clinic.clientmanager.entity.CardsEntity;
import net.clinic.clientmanager.entity.DoctorsEntity;
import net.clinic.clientmanager.entity.UserAuthenticationEntity;
import net.clinic.clientmanager.service.UserAuthenticationService;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;


@Component
public class UserAuthenticationServiceImpl implements UserAuthenticationService {

    @PersistenceContext
    EntityManager em;

    private final String EXAMINED = "Осмотрен";
    private final String ENTERED = "Поступил";
    private final String RECOVERY = "Выздоравливает";


    @Transactional
    @Override
    public void createUser(UserAuthenticationEntity user) {

    }

    @Transactional
    @Override
    public void delereUser(Integer id) {

    }

    @Transactional
    @Override
    public void blockUser(Integer userId) {
        em.find(UserAuthenticationEntity.class, userId).setEnabled(false);
    }

    @Transactional
    @Override
    public void unlockUser(Integer userId) {
        em.find(UserAuthenticationEntity.class, userId).setEnabled(true);
    }

    @Transactional
    @Override
    public void distributionCards(Integer userId) {
        int cards;
        int doctors;
        DoctorsEntity doctor = em.find(DoctorsEntity.class, userId);
        List<CardsEntity> cardsForDistribute = new ArrayList<>();
        List<CardsEntity> cardsEntityList = em.createQuery("select c from CardsEntity c where c.docId = :doctor").setParameter("doctor", doctor).getResultList();
        List<DoctorsEntity> list = em.createQuery("select d from DoctorsEntity d ").getResultList();
        if (cardsEntityList != null) {
            for (CardsEntity cardsEntity : cardsEntityList) {
                if (cardsEntity.getStatus().equals(ENTERED) || cardsEntity.getStatus().equals(EXAMINED) || cardsEntity.getStatus().equals(RECOVERY)) {
                    cardsForDistribute.add(cardsEntity);
                }
            }
            cards = cardsForDistribute.size();
            int boss = doctor.getBoss();
            List<DoctorsEntity> doctorsEntityList = em.createQuery("select d from DoctorsEntity d where d.boss = :boss").setParameter("boss", boss).getResultList();
            doctors = doctorsEntityList.size();
            distributeCards(cards, doctors, cardsForDistribute, doctorsEntityList);
        }
    }

    @Transactional
    private void distributeCards(int cards, int doctors, List<CardsEntity> cardsForDistribute, List<DoctorsEntity> doctorsEntityList) {
        int iCards = 0;
        int iDoc = 0;
        while (iCards != cards) {
            if (iDoc == doctors) {
                iDoc = 0;
            }
            cardsForDistribute.get(iCards).setDocId(doctorsEntityList.get(iDoc));
            iCards++;
            iDoc++;
        }
    }

    @Override
    public UserAuthenticationEntity findUser(Integer id) {
        return null;
    }

    @Override
    public List<UserAuthenticationEntity> findAllUsers() {
        return em.createQuery("select u from UserAuthenticationEntity u where u.docId IN (select u.docId from UserAuthorizationEntity u where u.role='ROLE_USER')").getResultList();
    }

    @Override
    public UserAuthenticationEntity findByNickname(String nickname) {

        return (UserAuthenticationEntity) em.createQuery("select u from UserAuthenticationEntity u where u.username =:nickname").setParameter("nickname", nickname).getSingleResult();
    }
}
