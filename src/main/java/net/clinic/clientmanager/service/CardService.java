package net.clinic.clientmanager.service;


import net.clinic.clientmanager.entity.CardsEntity;

import java.util.List;

public interface CardService {
    public void createCard(CardsEntity card);

    public void deleteCard(Integer id);

    public CardsEntity findCard(Integer id);

    public List<CardsEntity> findAllCard();
}
