package net.clinic.clientmanager.service;

import net.clinic.clientmanager.entity.StatusEntity;

import java.util.List;

public interface StatusService {

    public void addStatus(StatusEntity statusEntity);

    public List<StatusEntity> findAll();

    public void delete(Integer id);
}
