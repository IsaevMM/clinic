package net.clinic.clientmanager;

import net.clinic.clientmanager.entity.*;

import java.util.List;


public class Wrapper {

    private List<TypesEntity> typesEntityList;
    private List<MedicineEntity> medicineEntityList;
    private List<DoctorsEntity> doctorsEntityList;
    private List<CardsEntity> cardsEntityList;
    private List<StatusEntity> statusEntityList;
    private String user;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public List<TypesEntity> getTypesEntityList() {
        return typesEntityList;
    }

    public void setTypesEntityList(List<TypesEntity> typesEntityList) {
        this.typesEntityList = typesEntityList;
    }

    public List<MedicineEntity> getMedicineEntityList() {
        return medicineEntityList;
    }

    public void setMedicineEntityList(List<MedicineEntity> medicineEntityList) {
        this.medicineEntityList = medicineEntityList;
    }

    public List<DoctorsEntity> getDoctorsEntityList() {
        return doctorsEntityList;
    }

    public void setDoctorsEntityList(List<DoctorsEntity> doctorsEntityList) {
        this.doctorsEntityList = doctorsEntityList;
    }

    public List<CardsEntity> getCardsEntityList() {
        return cardsEntityList;
    }

    public void setCardsEntityList(List<CardsEntity> cardsEntityList) {
        this.cardsEntityList = cardsEntityList;
    }

    public List<StatusEntity> getStatusEntityList() {
        return statusEntityList;
    }

    public void setStatusEntityList(List<StatusEntity> statusEntityList) {
        this.statusEntityList = statusEntityList;
    }
}
