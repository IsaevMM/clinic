package net.clinic.clientmanager.web;


import net.clinic.clientmanager.Wrapper;
import net.clinic.clientmanager.entity.*;
import net.clinic.clientmanager.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
public class ClinicController {


    @Autowired
    CardService cardService;
    @Autowired
    DoctorService doctorService;
    @Autowired
    TypeService typeService;
    @Autowired
    MedicineService medicineService;
    @Autowired
    StatusService statusService;
    @Autowired
    UserAuthenticationService userAuthenticationService;

    @PersistenceContext
    EntityManager em;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public void redirect(HttpServletResponse response) throws IOException {
        response.sendRedirect("login.html");
    }


    @RequestMapping(value = "/getData", method = RequestMethod.GET)
    public Wrapper getData() {


        List<TypesEntity> typesEntityList = typeService.findAll();
        List<MedicineEntity> medicineEntityList = medicineService.findAll();
        List<DoctorsEntity> doctorsEntityList = doctorService.findAll();
        List<CardsEntity> cardsEntityList = cardService.findAllCard();
        List<StatusEntity> statusEntityList = statusService.findAll();
        String username = SecurityContextHolder.getContext().getAuthentication().getName();

        UserAuthenticationEntity user = userAuthenticationService.findByNickname(username);
        List<DoctorsEntity> userDoctors = new ArrayList<>();
        for (DoctorsEntity doctorsEntity : doctorsEntityList) {
            if (doctorsEntity.getBoss() == user.getDocId() || doctorsEntity.getDocId() == user.getDocId()) {
                userDoctors.add(doctorsEntity);
            }
        }


        Wrapper wrapper = new Wrapper();
        wrapper.setUser(username);
        wrapper.setCardsEntityList(cardsEntityList);
        wrapper.setDoctorsEntityList(userDoctors);
        wrapper.setMedicineEntityList(medicineEntityList);
        wrapper.setTypesEntityList(typesEntityList);
        wrapper.setStatusEntityList(statusEntityList);

        return wrapper;

    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public void register(@RequestParam("name") String name,
                         @RequestParam("type") String type,
                         @RequestParam("doctor") Integer doctorID,
                         @RequestParam("description") String description,
                         @RequestParam("status") String status,
                         @RequestParam("date") Long date,
                         @RequestParam("medicines") List<String> medicine) {
        String cure = "";
        if (medicine.size() != 0) {
            for (int i = 0; i < medicine.size(); i++) {
                cure += medicine.get(i) + ",";
            }
        }

        Date arrivedDate = new Date(date);
        DoctorsEntity doctorsEntity = doctorService.findById(doctorID);
        CardsEntity cardsEntity = new CardsEntity(arrivedDate, description, name, status, type, doctorsEntity, cure);
        cardService.createCard(cardsEntity);
    }

    @RequestMapping(value = "/find", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CardsEntity> find(@RequestParam("name") String name,
                                  @RequestParam("type") String type,
                                  @RequestParam("status") String status,
                                  @RequestParam("fromDate") Long fromDate,
                                  @RequestParam("toDate") Long toDate) {

        System.out.println(name);

        Predicate namePredicate;
        Predicate typePredicate;
        Predicate statusPredicate;


        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<CardsEntity> entityCriteriaQuery = cb.createQuery(CardsEntity.class);
        Root<CardsEntity> c = entityCriteriaQuery.from(CardsEntity.class);
        List<Predicate> myPredicates = new ArrayList<>();

        if (!name.equals("")) {
            System.out.println(name);
            namePredicate = cb.equal(c.get("name"), name);
            myPredicates.add(namePredicate);
        }
        if (!type.equals("")) {
            System.out.println(type);
            typePredicate = cb.equal(c.get("type"), type);
            myPredicates.add(typePredicate);
        }
        if (!status.equals("")) {
            statusPredicate = cb.equal(c.get("status"), status);
            myPredicates.add(statusPredicate);
        }


        if (fromDate > 0) {
            Date fromdate = new Date(fromDate);
            Predicate startPredicate = cb.greaterThanOrEqualTo(c.<Date>get("date"), fromdate);
            myPredicates.add(startPredicate);
        }

        if (toDate > 0) {
            Date todate = new Date(toDate);
            Predicate endPredicate = cb.lessThanOrEqualTo(c.<Date>get("date"), todate);
            myPredicates.add(endPredicate);
        }
        int length = myPredicates.size();
        Predicate predicates[] = new Predicate[length];
        for (int i = 0; i < predicates.length; i++) {
            predicates[i] = myPredicates.get(i);
        }

        entityCriteriaQuery.where(predicates);
        TypedQuery<CardsEntity> query = em.createQuery(entityCriteriaQuery);
        return query.getResultList();
    }

    @RequestMapping(value = "/getAllRecord", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Wrapper getAllRecord() {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        List<TypesEntity> typesEntityList = typeService.findAll();
        List<CardsEntity> cardsEntityList = cardService.findAllCard();
        List<StatusEntity> statusEntityList = statusService.findAll();

        Wrapper wrapper = new Wrapper();
        wrapper.setUser(username);
        wrapper.setCardsEntityList(cardsEntityList);
        wrapper.setTypesEntityList(typesEntityList);
        wrapper.setStatusEntityList(statusEntityList);

        return wrapper;
    }

    @RequestMapping(value = "/remove", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public void remove(@RequestParam("cardId") Integer id) {
        cardService.deleteCard(id);
    }

    @RequestMapping(value = "/addType", method = RequestMethod.POST)
    public void addType(@RequestParam("type") String type) {
        TypesEntity entity = new TypesEntity();
        entity.setType(type);
        typeService.addType(entity);

    }

    @RequestMapping(value = "/addMedicine", method = RequestMethod.POST)
    public void addMedicine(@RequestParam("cure") String cure) {
        MedicineEntity medicineEntity = new MedicineEntity();
        medicineEntity.setName(cure);
        medicineService.addMedicine(medicineEntity);

    }

    @RequestMapping(value = "/getTypes", method = RequestMethod.GET)
    public List<TypesEntity> getTypes() {
        return typeService.findAll();
    }

    @RequestMapping(value = "/getMedicine", method = RequestMethod.GET)
    public List<MedicineEntity> getMedicine() {
        return medicineService.findAll();
    }


    @RequestMapping(value = "/getUsers", method = RequestMethod.GET)
    public List<UserAuthenticationEntity> getUsers() {
        return userAuthenticationService.findAllUsers();
    }

    @RequestMapping(value = "/blockUser", method = RequestMethod.GET)
    public void blockUser(@RequestParam("id") Integer userId) {

        userAuthenticationService.blockUser(userId);
        userAuthenticationService.distributionCards(userId);
    }

    @RequestMapping(value = "/unlockUser", method = RequestMethod.GET)
    public void unlockUser(@RequestParam("id") Integer userId) {

        userAuthenticationService.unlockUser(userId);

    }

    @RequestMapping(value = "/subordinates", method = RequestMethod.GET)
    public Wrapper getSubordinates(@RequestParam("id") Integer docId) {
        System.out.println(docId);

        List<DoctorsEntity> subList = new ArrayList<>();
        List<CardsEntity> cardsSubList = new ArrayList<>();
        List<CardsEntity> cardsList = cardService.findAllCard();
        List<DoctorsEntity> doctorsEntityList = doctorService.findAll();
        for (DoctorsEntity doctorsEntity : doctorsEntityList) {
            if (doctorsEntity.getBoss() == docId) {
                subList.add(doctorsEntity);
            }
        }
        for (CardsEntity cardsEntity : cardsList) {
            for (DoctorsEntity doctorsEntity : subList) {
                if (cardsEntity.getDocId().equals(doctorsEntity)) {
                    cardsSubList.add(cardsEntity);
                }
            }
        }
        Wrapper wrapper = new Wrapper();
        wrapper.setCardsEntityList(cardsSubList);
        wrapper.setDoctorsEntityList(subList);
        return wrapper;

    }

    @RequestMapping(value = "/changeDoctor", method = RequestMethod.POST)
    @Transactional
    public void changeDoctor(@RequestParam("id") Integer docId,
                             @RequestParam("cardId") Integer cardId) {
        DoctorsEntity newDoctor = doctorService.findById(docId);
        cardService.findCard(cardId).setDocId(newDoctor);

    }

}
