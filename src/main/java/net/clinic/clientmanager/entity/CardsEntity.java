package net.clinic.clientmanager.entity;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name = "Cards", schema = "dbo", catalog = "Clinic")
public class CardsEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "CARD_ID")
    private Integer cardId;

    public Integer getCardId() {
        return cardId;
    }

    public CardsEntity() {
    }

    public CardsEntity(Date date, String description, String name, String status, String type, DoctorsEntity docId, String medicine) {
        this.date = date;
        this.description = description;
        this.name = name;
        this.status = status;
        this.type = type;
        this.docId = docId;
        this.medicine = medicine;
    }

    public void setCardId(Integer cardId) {
        this.cardId = cardId;
    }

    @Column(name = "date")
    private Date date;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Column(name = "description", length = 500)
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "name")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "status")
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Column(name = "type")
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Column(name = "medicine",length = 500)
    private String medicine;

    public String getMedicine() {
        return medicine;
    }

    public void setMedicine(String medicine) {
        this.medicine = medicine;
    }

    @ManyToOne
    @JoinColumn(name = "DOC_ID")
    private DoctorsEntity docId;

    public DoctorsEntity getDocId() {
        return docId;
    }

    public void setDocId(DoctorsEntity docId) {
        this.docId = docId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CardsEntity that = (CardsEntity) o;

        if (cardId != that.cardId) return false;
        if (date != null ? !date.equals(that.date) : that.date != null) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (status != null ? !status.equals(that.status) : that.status != null) return false;
        if (type != null ? !type.equals(that.type) : that.type != null) return false;
        if (docId != null ? !docId.equals(that.docId) : that.docId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = cardId;
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (docId != null ? docId.hashCode() : 0);
        return result;

    }
}
