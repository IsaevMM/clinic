package net.clinic.clientmanager.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;


@Entity
@Table(name = "Doctors")
public class DoctorsEntity {


    @Id
    @GenericGenerator(name = "generator", strategy = "increment")
    @GeneratedValue(generator = "generator")
    @Column(name = "DOC_ID")
    private Integer docId;

    public Integer getDocId() {
        return docId;
    }

    public void setDocId(Integer docId) {
        this.docId = docId;
    }

    @Column(name = "Lastname")
    private String lastname;

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String laststname) {
        this.lastname = laststname;
    }

    @Column(name = "Firstname")
    private String firstname;

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    @Column(name = "Midlename")
    private String midlename;

    public String getMidlename() {
        return midlename;
    }

    public void setMidlename(String midlename) {
        this.midlename = midlename;
    }

    @Column(name = "Boss")
    private Integer boss;

    public Integer getBoss() {
        return boss;
    }

    public void setBoss(Integer boss) {
        this.boss = boss;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DoctorsEntity that = (DoctorsEntity) o;

        if (docId != that.docId) return false;
        if (lastname != null ? !lastname.equals(that.lastname) : that.lastname != null) return false;
        if (firstname != null ? !firstname.equals(that.firstname) : that.firstname != null) return false;
        if (midlename != null ? !midlename.equals(that.midlename) : that.midlename != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = docId;
        result = 31 * result + (lastname != null ? lastname.hashCode() : 0);
        result = 31 * result + (firstname != null ? firstname.hashCode() : 0);
        result = 31 * result + (midlename != null ? midlename.hashCode() : 0);
        return result;
    }
}
