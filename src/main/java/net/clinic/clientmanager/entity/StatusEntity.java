package net.clinic.clientmanager.entity;


import javax.persistence.*;

@Entity
@Table(name = "States")
public class StatusEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "STATE_ID")
    private Integer state_id;

    public Integer getState_id() {
        return state_id;
    }

    public void setState_id(Integer id) {
        this.state_id = id;
    }

    @Column(name = "state")
    private String state;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
