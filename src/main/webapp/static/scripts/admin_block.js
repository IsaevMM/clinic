var app = angular.module("blockModule", []);

app.controller('blockCtrl', function ($scope, $http) {

    $scope.users = [];

    $scope.isDisabled = [];

    $scope.getUsers = function () {

        $http({
            method: 'GET',
            url: '/getUsers'
        }).then(function successCallback(response) {
            $scope.users = response.data;

        }, function errorCallback(response) {

        });

    };

    $scope.init = function () {
        $scope.getUsers();
    };
    $scope.isDisabledButton = function () {
        $scope.isDisabled = true;
    };

    $scope.blockUser = function (userId) {

        $http({
            method: 'GET',
            url: '/blockUser',
            params: {"id": userId}
        }).then(function successCallback(response) {
            alert("Пользователь заблокирован");
            window.location.reload();

        }, function errorCallback(response) {
            alert("Что-то пошло не так");

        });
    };
    $scope.unlockUser = function (userId) {
        $http({
            method: 'GET',
            url: '/unlockUser',
            params: {"id": userId}
        }).then(function successCallback(response) {
            alert("Пользователь разблокирован");
            window.location.reload();

        }, function errorCallback(response) {
            alert("Что-то пошло не так");

        });
    }
});