'use strict';
var postApp = angular.module('postApp', ["checklist-model"]);


postApp.controller('postController', function ($scope, $http) {

    var typesFromServer = [];
    var doctorsFromServer = [];
    var statesFromServer = [];
    var curesFromServer = [];
    $scope.states = [];
    $scope.doctors = [];
    $scope.cures = [];
    $scope.types = [];
    $scope.user = '';

    $scope.card = {};
    $scope.card.cure = [];
    $scope.card.date = new Date();

    $scope.createTypes = function (types) {
        for (var i = 0; i < types.length; i++) {
            $scope.types.push(types[i].type)
        }
    };
    $scope.createStates = function (states) {
        for (var i = 0; i < states.length; i++) {
            $scope.states.push(states[i].state)
        }
    };
    $scope.createMedicine = function (cures) {
        for (var i = 0; i < cures.length; i++) {
            $scope.cures.push(cures[i].name)
        }
    };
    $scope.createDoctors = function (doctors) {
        for (var i = 0; i < doctors.length; i++) {
            $scope.doctors.push({"id": doctors[i].docId, "FIO": doctors[i].lastname + " " + doctors[i].firstname + " " + doctors[i].midlename});
        }
    };


    $scope.submit = function () {

        if($scope.cures.cure === undefined){
            $scope.cures.cure = "";
        }
        $http({
            method: 'POST',
            url: '/register',
            params: {
                "name": $scope.card.name,
                "type": $scope.card.type,
                "doctor": $scope.card.doctor,
                "status": $scope.card.status,
                "description": $scope.card.description,
                "date": $scope.card.date.getTime(),
                "medicines": $scope.cures.cure
            }
        })
            .success(function () {
                alert('success');
            })
            .error(function () {
                alert('error');
            })
    };


    $scope.cure = {
        cures: ['cure']
    };

    $scope.checkAll = function () {
        $scope.cures.cure = angular.copy($scope.cure);
    };
    $scope.uncheckAll = function () {
        $scope.cures.cure = [];
    };
    $scope.checkFirst = function () {
        $scope.cures.cure.splice(0, $scope.cures.cure.length);
    };


    $scope.getDataFromServer = function () {

        $http({
            method: 'GET',
            url: '/getData'
        }).then(function successCallback(response) {
            var wrapData = response.data;
            $scope.user = wrapData.user;

            typesFromServer = wrapData.typesEntityList;
            doctorsFromServer = wrapData.doctorsEntityList;
            statesFromServer = wrapData.statusEntityList;
            curesFromServer = wrapData.medicineEntityList;
            $scope.createDoctors(doctorsFromServer);
            $scope.createTypes(typesFromServer);
            $scope.createMedicine(curesFromServer);
            $scope.createStates(statesFromServer);

        }, function errorCallback(response) {

        });
    };


});



