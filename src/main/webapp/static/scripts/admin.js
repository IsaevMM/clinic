'use strict';
var adminApp = angular.module('adminApp', []);

adminApp.controller('adminController', function ($scope, $http) {

    $scope.types = [];
    $scope.medicines = [];

    $scope.addTypeRecord = function () {

        var str = $scope.typeName.charAt(0).toUpperCase() + $scope.typeName.substring(1);
        if (!isTypeExist(str)) {
            sendTypeData(str)
        } else {
            alert("Этот тип существует");
        }
    };

    function isTypeExist(tpName) {
        for (var i = 0; i < $scope.types.length; i++) {
            if ($scope.types[i].type === tpName) {
                return true;
            }
        }
        return false;
    }

    function sendTypeData(name) {
        $http({
            method: "POST",
            url: "/addType",
            params: {"type": name}
        })
            .success(function () {
                alert('success');
            })
            .error(function () {
                alert('error');
            })
    }


    $scope.getData = function () {

        $http({
            method: 'GET',
            url: '/getTypes'
        }).then(function successCallback(response) {
            $scope.types = response.data;

        }, function errorCallback(response) {

        });
        $scope.getMedicines();

    };

    $scope.getMedicines = function () {

        $http({
            method: 'GET',
            url: '/getMedicine'
        }).then(function successCallback(response) {
            $scope.medicines = response.data;

        }, function errorCallback(response) {

        });
    };


    $scope.addMedicine = function () {
        var str = $scope.cureName.toLowerCase();
        if (!isExistMedicine(str)) {
            sendMedicineData(str)
        } else {
            alert("Это лекарство существует");
        }
    };


    function sendMedicineData(name) {
        $http({
            method: "POST",
            url: "/addMedicine",
            params: {"cure": name}
        })
            .success(function () {
                alert('success');
            })
            .error(function () {
                alert('error');
            })
    }


    function isExistMedicine(cureName) {
        for (var i = 0; i < $scope.medicines.length; i++) {
            var cureFromArray = $scope.medicines[i].name.toLowerCase();
            if (cureFromArray === cureName) {
                return true;
            }
        }
        return false;
    }

    $scope.reload = function () {
        window.location.reload();
    };




});