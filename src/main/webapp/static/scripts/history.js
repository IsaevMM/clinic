'use strict';
var getApp = angular.module('getApp', []);
var data;
getApp.controller('getController', function ($scope, $http) {

    $scope.cards = [];
    $scope.user = '';
    $scope.card = {
        name: "",
        type: "",
        status: "",
        fromDate: null,
        toDate: null
    };
    var statesFromServer = [];
    var typesFromServer = [];
    var cardsFromServer = [];
    $scope.types = [];
    $scope.states = [];
    $scope.addRow = function (data) {

        $scope.cards = [];
        for (var i = 0; i < data.length; i++) {

            $scope.cards.push({
                "ID": data[i].cardId,
                "number": i + 1,
                "name": data[i].name, "type": data[i].type, "description": data[i].description, "date": data[i].date,
                "doctor": data[i].docId.lastname + " " + data[i].docId.firstname + " " + data[i].docId.midlename, "status": data[i].status,
                "medicines": data[i].medicine
            });

            $scope.number = '';
            $scope.name = '';
            $scope.type = '';
            $scope.description = '';
            $scope.date = '';
            $scope.doctor = '';
            $scope.status = '';
            $scope.medicines = '';
        }

    };

    $scope.createTypes = function (types) {
        for (var i = 0; i < types.length; i++) {
            $scope.types.push(types[i].type)
        }
    };
    $scope.createStates = function (states) {
        for (var i = 0; i < states.length; i++) {
            $scope.states.push(states[i].state)
        }
    };


    $scope.getAllRecord = function () {
        $http({
            method: 'GET',
            url: '/getAllRecord'
        }).then(function successCallback(response) {
            data = response.data;
            $scope.user = data.user;
            cardsFromServer = data.cardsEntityList;
            typesFromServer = data.typesEntityList;
            statesFromServer = data.statusEntityList;
            $scope.createTypes(typesFromServer);
            $scope.createStates(statesFromServer);
            $scope.addRow(cardsFromServer);

        }, function errorCallback(response) {

        });
    };

    $scope.init = function () {
        $scope.getAllRecord();
    };


    $scope.refresh = function () {
        window.location.reload();
    };

    $scope.find = function () {
        var sendDateFrom;
        var sendDateTo;
        if ($scope.card.fromDate === null) {
            sendDateFrom = 0
        } else {
            sendDateFrom = $scope.card.fromDate.getTime();
        }
        if ($scope.card.toDate === null) {
            sendDateTo = 0;
        } else {
            sendDateTo = $scope.card.toDate.getTime();
        }
        $http({
            method: 'GET',
            url: '/find',
            params: {
                "name": $scope.card.name,
                "type": $scope.card.type,
                "status": $scope.card.status,
                "fromDate": sendDateFrom,
                "toDate": sendDateTo
            }
        }).then(function successCallback(response) {
            data = response.data;
            $scope.addRow(data);

        }, function errorCallback(response) {

        });
        $scope.card.name = "";
        $scope.card.type = "";
        $scope.card.status = "";
        $scope.card.fromDate = null;
        $scope.card.toDate = null;
    };

    $scope.changeRecord = function () {
    };


    $scope.removeRecord = function (ID) {
        if (confirm("Вы точно хотите удалить запись?")) {
            var index = -1;
            var comArr = eval($scope.cards);
            for (var i = 0; i < comArr.length; i++) {
                if (comArr[i].ID === ID) {
                    index = i;
                    break;
                }
            }
            if (index === -1) {
                alert("Something wrong");
            }
            $http({
                method: 'POST',
                url: '/remove',
                params: {
                    "ID": ID
                }
            });

            $scope.cards.splice(index, 1);
        }
    };


});
