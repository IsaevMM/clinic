'use strict';
var empApp = angular.module('empApp', []);
empApp.controller('empController', function ($scope, $http) {


    var doctorsFromServer = [];

    $scope.doctors = [];
    $scope.user = '';
    $scope.docId = 0;
    $scope.newDocId = 0;
    $scope.status = '';
    $scope.changingCardId = 0;
    $scope.isActive = false;
    $scope.isDisabled = false;
    $scope.subs = [];
    $scope.id = 0;
    $scope.cardsOfMySubordinates = [];

    $scope.getEmployee = function () {

        $http({
            method: 'GET',
            url: '/getData'
        }).then(function successCallback(response) {
            var wrapData = response.data;
            $scope.user = wrapData.user;
            doctorsFromServer = wrapData.doctorsEntityList;
            $scope.createDoc(doctorsFromServer, $scope.doctors);
        }, function errorCallback(response) {

        });
    };

    $scope.createDoc = function (doctors, array) {
        for (var i = 0; i < doctors.length; i++) {
            array.push({"id": doctors[i].docId, "FIO": doctors[i].lastname + " " + doctors[i].firstname + " " + doctors[i].midlename});
        }
    };

    $scope.getSubordinate = function () {
        $http({
            method: 'GET',
            url: '/subordinates',
            params: {"id": $scope.id}
        }).then(function successCallback(response) {
            $scope.cardsOfMySubordinates = response.data.cardsEntityList;
            if ($scope.cardsOfMySubordinates.length == 0) {
                alert("У этого доктора нет подчиненных");
            }
        }, function errorCallback(response) {

        });

    };

    $scope.changeDoctor = function (cardId) {

        $scope.isDisabled = true;
        $scope.isActive = true;
        $scope.changingCardId = cardId;
    };
    $scope.saveDoc = function () {
        $http({
            method: 'POST',
            url: '/changeDoctor',
            params: {
                "id": $scope.newDocId,
                "cardId": $scope.changingCardId
            }
        }).then(function successCallback(response) {
            $scope.isDisabled = false;
            $scope.isActive = false;
            window.location.reload();
        }, function errorCallback(response) {

        });

    };


});